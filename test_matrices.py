from classes.matrices import Matrices
from classes.param import Param
import unittest
import numpy as np
from scipy import linalg
import scipy.sparse.linalg as spla


class MatricesTestCase(unittest.TestCase):
	def __init__(self, *args, **kwargs):
		unittest.TestCase.__init__(self, *args, **kwargs)
		
		self.par = Param('tests')
		self.mat = Matrices(self.par)
	
	def test_Weight_const(self):
		"""Check the weight function with a constant concentration """
		v = np.ones(self.par.N1)/self.par.Lx
		np.testing.assert_almost_equal(self.par.Weight.dot(v[:-1]), 1)
	
	def test_DiffusionNeumann(self):
		""" Test if the Diffusion Operator for the no-flux BC of the
			concentration fullfills the requirements :
			-no leak through the boundary: sum(v_start) == sum(v_end)
			-must be homogeneous at the end"""
		
		v = np.ones(self.par.N1)*2+np.sin(self.par.x)*np.exp(np.cos(self.par.x))
		Sum = self.par.Weight.dot(v[:-1])
		PreC = self.mat.Prec(self.mat.DC)
		
		for i in range(2500):
			vn, info = spla.gmres(self.mat.DC, v, tol=1e-10, M=PreC, atol=1e-10)
			vn, v = v, vn
		
		np.testing.assert_almost_equal(Sum, self.par.Weight.dot(v[:-1]))
		np.testing.assert_almost_equal(np.mean(v), v[2])

if __name__ == '__main__':
	unittest.main()
