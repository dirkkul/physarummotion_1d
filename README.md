# Introduction

This code can be used to study the onset of motion in physarum microplasmodia using python/numpy/scipy. Depending on the model parameters, you can observe oscillatory and irregular motion. The details of the model can be found [here](http://iopscience.iop.org/article/10.1088/1361-6463/aae41d) . Feel free to use this code for your own projects, but please cite  
`Dirk Alexander Kulawiak et al 2019 J. Phys. D: Appl. Phys. 52 014004`.

# Usage

Clone this reposity. Set the model parameters in the `StartSim.py` script can execute it. This script can also be used to scan parameter ranges etc. The `Example_param.ini` file contains the paramters we used in our paper. 