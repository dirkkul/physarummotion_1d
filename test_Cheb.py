import unittest
import sympy as sp
import numpy as np
from classes.Cheb import cheb

class ChebTestCase(unittest.TestCase):
	"""Tests for `Cheb.py`."""
	
	def test_cheb_n2_01(self):
		"""Is five successfully determined to be prime?"""
		N=2
		(CD, CD2,r,x)  = cheb(N,0,1)
		
		#from Trefethen
		Diff = np.array( [[1.5,-2.,0.5],[0.5,0,-0.5],[-0.5,2,-1.5]])*2
		
		self.assertEqual(N+1,len(r))
		np.testing.assert_almost_equal(x, [1,0.,-1])
		np.testing.assert_almost_equal(r, [1,0.5,0])
		np.testing.assert_almost_equal(CD, Diff)
	
	def test_cheb_n2_02(self):
		"""Is five successfully determined to be prime?"""
		N=2
		(CD, CD2,r,x)  = cheb(N,0,2)
		
		#from Trefethen
		Diff = np.array( [[1.5,-2.,0.5],[0.5,0,-0.5],[-0.5,2,-1.5]])
		
		self.assertEqual(N+1,len(r))
		np.testing.assert_almost_equal(x, [1,0.,-1])
		np.testing.assert_almost_equal(r, [2,1.,0])
		np.testing.assert_almost_equal(CD, Diff)
		
	def test_cheb_n4_01(self):
		"""Is five successfully determined to be prime?"""
		N=4
		(CD, CD2,r,x)  = cheb(N,0,1)
		
		#from Trefethen
		Diff = np.array( [[5.5,-6.8284,2.0,-1.1716,0.5],[1.7071,-0.7071,-1.4142,0.7071,-0.2929],
							[-0.5,1.4142,0.,-1.4142,0.5],[0.2929,-0.7071,1.4142,0.7071,-1.7071],
							[-0.5,1.1716,-2.0,6.8284,-5.5]])*2
		
		self.assertEqual(N+1,len(r))
		np.testing.assert_almost_equal(x, [1.,0.70710678,0.,-0.70710678,-1.])
		np.testing.assert_almost_equal(r, [1., 0.8535534, 0.5, 0.1464466, 0.])
		np.testing.assert_almost_equal(CD, Diff,4)
		
	def test_cheb_derivatives(self):
		#create derivatives
		r = sp.symbols("r")
		
		sin = sp.diff(sp.sin(r), r, 1)
		sin_ana = sp.lambdify(r, sin, "numpy")
		sin2 = sp.diff(sp.sin(r), r, 2)
		sin2_ana = sp.lambdify(r, sin2, "numpy")
		
		sinE = sp.diff(sp.sin(r)*sp.functions.exp(r), r, 1)
		sinE_ana = sp.lambdify(r, sinE, "numpy")
		sinE2 = sp.diff(sp.sin(r)*sp.functions.exp(r), r, 2)
		sinE2_ana = sp.lambdify(r, sinE2, "numpy")
		
		N=21
		(CD, CD2,r,x)  = cheb(N,0,1)
		
		sin_cheb = np.dot(CD,np.sin(r))
		sin2_cheb = np.dot(CD2,np.sin(r))
		sinE_cheb = np.dot(CD,np.sin(r)*np.exp(r))
		sinE2_cheb = np.dot(CD2,np.sin(r)*np.exp(r))
		
		np.testing.assert_almost_equal(sin_cheb, sin_ana(r))
		np.testing.assert_almost_equal(sinE_cheb, sinE_ana(r))
		np.testing.assert_almost_equal(sin2_cheb, sin2_ana(r))
		np.testing.assert_almost_equal(sinE2_cheb, sinE2_ana(r))


if __name__ == '__main__':
	unittest.main()
