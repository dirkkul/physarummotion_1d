import numpy as np
from classes.Cheb import cheb
from scipy import linalg
import scipy.sparse.linalg as spla

class Matrices:
	def __init__(self, par):
		
		(self.CD, self.CD2, par.x, xx) = cheb(par.N, 0, par.Lx)
		DN = np.copy(self.CD)
		DN[0] = np.zeros(par.N1)
		DN[-1] = np.zeros(par.N1)
		self.D2N = np.dot(self.CD, DN)
		
		self.DC = np.eye(par.N1) - self.D2N * par.Dcon*par.dt
		self.Ddt = self.CD* par.dt
		
		par.Weight = linalg.pinv2(self.CD[:par.N,:par.N] )[0,:]
		self.par = par


	def Prec(s, A):
		""" Create an ILU preconditioner for a Matrix """
		MC2 = spla.spilu(A)
		def MC_x(x): return MC2.solve(x)
		return spla.LinearOperator((np.shape(A)[0], np.shape(A)[1]), MC_x)


	def Factor(self, A, IndI, IndJ, val):
		""" Add a Diagonal entry to a submatrix. """
		II = self.par.N1 * IndI
		JJ = self.par.N1 * IndJ
		for i in range(self.par.N1):
			A[II + i,JJ + i] += val


	def Dx(self, A, IndI, IndJ, D, val):
		""" Add a matrix (usually a derivative) to a submatrix and
		multiply it with a parameter. """
		II = self.par.N1 * IndI
		JJ = self.par.N1 * IndJ
		A[II:II + self.par.N1, JJ:JJ + self.par.N1] += D * val


	def ResetLine(self, A, II):
		""" Reset a complete line to zero. """
		A[II, :] = 0.
