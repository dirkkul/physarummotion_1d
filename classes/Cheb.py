import numpy as np

def cheb(N, x0 = 0.1, x1 = 1):
	D = np.empty((N+1, N+1))
	D[0,0] = (2. * N**2 + 1. )/6.
	D[N,N] = -(2. * N**2 + 1. )/6.
	
	x = np.empty(N+1)
	for i in range(N+1):
		x[i] = np.cos((i * np.pi)/N) 
	
	for i in range(1,N):
		D[i,i] = -x[i]/(2.*(1.-x[i]**2))

	for i in range(0,N+1):
		for j in range(0,N+1):
			if i != j:
				ci = 1.
				cj = 1.
				if i == 0 or i == N :
					ci = 2.
				if j == 0 or j == N :
					cj = 2.
				D[i,j] = (-1.)**(i+j) *ci / (cj * (x[i] - x[j]))
	
	y = np.empty(N+1)
	for i in range(N+1):
		y[i] = (x0+x1)/2. + (x1-x0)/2.*x[i]
	
	D1 = 2./(x1-x0) *D
	D2 = np.dot(D1,D1)
	
	return (D1, D2, y, x)
