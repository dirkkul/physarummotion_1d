import configparser
import os
import time


class Param:
  """ Read all parameters from a param.ini file which
  needs to be located in the given path"""

  def __init__(self, path):
    self.path = path

    # Read all parameters from the ini file
    parser = configparser.SafeConfigParser()
    parser.read(os.path.join(path, 'param.ini'))
    
    # Numerics
    self.N = parser.getint('main', 'N')
    assert (self.N > 1),"More than N = 1 points needed!"
    self.dt = parser.getfloat('main', 'dt')
    assert (self.dt > 0),"Timestep dt must be positive!"
    
    # parameters
    self.Lx = parser.getfloat('main', 'LengthX')
    assert (self.Lx > 0),"Length must be positive!"
    
    self.Dcon = parser.getfloat('main', 'Dcon')
    self.VisGel = parser.getfloat('main', 'VisGel')
    self.VisSol = parser.getfloat('main', 'VisSol')
    self.FracGel = parser.getfloat('main', 'FracGel')
    self.beta = parser.getfloat('main', 'beta')
    self.mu = parser.getfloat('main', 'mu')
    self.xi = parser.getfloat('main', 'xi')
    self.gammaG = parser.getfloat('main', 'gammaG')
    
    self.EndTime = parser.getfloat('main', 'EndTime')
    self.SaveTime = parser.getfloat('main', 'SaveTime')
    self.Bound = parser.getint('main', 'Bound')
    
    # Options
    self.tol = parser.getfloat('main', 'tol')
    self.PlotStates = parser.getboolean('main', 'PlotStates')
    self.seed = parser.getint('main', 'seed')
    
    # compute a few helpful expressions
    self.FracSol = 1. - self.FracGel
    self.Steps = int(self.EndTime/self.dt)+1
    self.SaveStep = int(self.SaveTime/self.dt)
    self.PosSaveStep = self.SaveStep/10
    self.N1 = self.N + 1
    self.start = time.clock()
