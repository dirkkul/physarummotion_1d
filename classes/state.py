import numpy as np
import os
import pickle
import time

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable

class State:
    def __init__(s, par):
        # The current state
        s.c = np.ones(par.N1) + 0.001*np.random.rand(par.N1)
        s.w = np.zeros(par.VarNum * par.N1)
        s.rhs = np.zeros(par.VarNum * par.N1)
        
        # Save the various quantities on given timesteps
        s.SaveC = np.zeros((np.int(par.Steps//par.SaveStep)+2, par.N1))
        s.SaveW = np.zeros((np.int(par.Steps//par.SaveStep)+2, par.VarNum*par.N1))
        s.SaveT = np.zeros((np.int(par.Steps//par.SaveStep)+2, par.N1))
        
        # Save the position (Left edge and COM) over time
        s.Pos = np.zeros(np.int(par.Steps//par.PosSaveStep)+1)
        s.PosCOM = np.zeros(np.int(par.Steps//par.PosSaveStep)+1)
        
        s.C_detail = np.zeros(np.int(par.Steps//par.PosSaveStep)+1)


    def SetRHS(s, par, CD, T):
        s.rhs[:par.N1] = s.w[par.N1:2*par.N1]/par.dt
        s.rhs[par.N1:2*par.N1] = -par.FracGel*np.dot(CD,T)
        if par.Bound == 0:
            s.rhs[par.N1] = -T[0] * par.FracGel
            s.rhs[2*par.N1-1] = -T[-1] * par.FracGel
            s.rhs[2*par.N1] = 0
            s.rhs[3*par.N1-1] = 0
        elif par.Bound == 1:
            s.rhs[par.N1] = 0
            s.rhs[2*par.N1-1] = 0
            s.rhs[2*par.N1] = 0
            s.rhs[3*par.N1-1] = 0


    def CheckC(s):
        """ Check if the concentration has negative values and end the simulation if this happens """
        if min(abs(s.c)) <0.:
            with open(os.path.join(path, 'belowneg2.WTF'), 'wb') as fin:
                fin.write("belowneg2")
            fin.close()
            exit(1)


    def SpaceTime(s, par):
        s.PlotSpaceTime(par, s.SaveC, 'Concentration $C(x)$', 'ST_C', s.SaveC.max(), s.SaveC.min())
        maxFlow = max(abs(s.SaveW[5:, 1:par.N1-1].flatten()))
        maxDispl = max(abs(s.SaveW[5:, par.N1:2*par.N1].flatten()))
        s.PlotSpaceTime(par, s.SaveW[:, 1:par.N1-1], 'Gel velocity $\dot{\mathbf{u}}$', 'ST_GEL', maxFlow, -maxFlow)
        s.PlotSpaceTime(par, s.SaveW[:, par.N1:2*par.N1], 'Gel displacement $\mathbf{u}$', 'ST_Displ', maxDispl, -maxDispl)


    def PlotSpaceTime(s, par, Save, title, SaveName, MIN, MAX):
        f, ax = plt.subplots(1, 1)
        ax.set_title(title, fontsize=25, y=1.02)
        im = ax.matshow(Save.T, vmin = MIN, vmax = MAX, origin='lower',interpolation='spline36', cmap=plt.get_cmap('coolwarm'), extent=[0,par.EndTime,0,par.Lx], aspect='auto')
        divider = make_axes_locatable(ax)
        cax = divider.append_axes("right", size="8%", pad=0.05)
        cbar = plt.colorbar(im, cax=cax)
        ax.xaxis.set_label_position('bottom')
        ax.xaxis.tick_bottom()
        ax.set_xlabel(r'Time in $s$', fontsize=30)
        ax.set_ylabel(r'Position in $\mu m$', fontsize=30)
        plt.tick_params(
            axis='x',      # changes apply to the x-axis
            which='both',      # both major and minor ticks are affected
            bottom=False,      # ticks along the bottom edge are off
            top=False,     # ticks along the top edge are off
            labelbottom=True) # labels along the bottom edge are off
        plt.tight_layout()
        plt.savefig(os.path.join(par.path,SaveName+'.png'),bbox_inches='tight')
        plt.close(f)


    def PlotStates(s, par, run):
        if not par.PlotStates:
            return
        
        f, ax = plt.subplots(1, 3, figsize=(3*5, 5))
        
        ax[0].set_title(r'Velocities', fontsize=25, y=1.04)
        ax[0].plot(par.x, s.w[:par.N1], label=r'$\dot{u}$', lw =2.)
        ax[0].plot(par.x, s.w[2*par.N1:3*par.N1], label=r'$v$', lw =2.)
        ax[0].plot(par.x, s.w[2*par.N1:3*par.N1]-s.w[:par.N1], 'r', label=r'$v - \dot{u}$', lw =2.)
        
        lgd = ax[0].legend(loc=1, borderaxespad=0., ncol=3)
        ax[1].set_title(r'Displacement $u(x)$', fontsize=33, y=1.04)
        ax[1].plot(par.x, s.w[1*par.N1:2*par.N1], lw =2.)
        ax[2].set_title(r'Concentration $C(x)$', fontsize=33, y=1.04)
        ax[2].plot(par.x, s.c, lw = 2.)
        
        plt.tight_layout()
        plt.savefig(os.path.join(par.path, 'State','PhysFull_'+str(run)+'.png'), bbox_inches='tight')
        plt.close(f)


    def PlotPos(s, par):
        TimeSteps = np.arange(0.,par.EndTime+par.dt,par.PosSaveStep*par.dt)
        f, ax = plt.subplots(1, 1)
        ax.set_title(r'Cell position', fontsize=25, y=1.02)
        ax.plot(TimeSteps, s.Pos ,label='Position' )
        ax.plot(TimeSteps, s.PosCOM , label='COM')
        ax.set_xlabel("Time in s", fontsize=30)
        ax.set_ylabel(r'X in $\mu m$', fontsize=30)
        plt.legend()
        plt.tight_layout()
        plt.savefig(os.path.join(par.path,'Pos.png'),bbox_inches='tight')
        plt.close(f)


    def DumpState(s, par):
        pickle.dump(s.Pos, open(os.path.join(par.path, 'Pos.pkl'), 'wb'), -1)
        pickle.dump(s.PosCOM,  open(os.path.join(par.path, 'PosCOM.pkl'), 'wb'), -1)
        pickle.dump(s.SaveT, open(os.path.join(par.path, 'SaveT.pkl'), 'wb'), -1)
        pickle.dump(s.SaveW, open(os.path.join(par.path, 'SaveW.pkl'), 'wb'), -1)
        pickle.dump(s.SaveC, open(os.path.join(par.path, 'Cdata.pkl'), 'wb'), -1)
        pickle.dump(s.C_detail, open(os.path.join(par.path, 'C_detail.pkl'), 'wb'), -1)
        
        pickle.dump(par.x, open(os.path.join(par.path,'points.pkl'), 'wb'), -1)


    def SaveStep(s, par, t, T):
        """ Save Position and values of the concentration and flow fields.
            The Position is saved more often as it needs much less memory. """
    
        if t % par.PosSaveStep == 0:
            s.Pos[np.int(t // par.PosSaveStep)] = s.w[par.N1]
            s.PosCOM[np.int(t // par.PosSaveStep)] = par.Weight.dot(s.w[par.N1:2*par.N1-1])/par.Lx
            s.C_detail[np.int(t // par.PosSaveStep)] = s.c[par.N1//2]
        
        if t % par.SaveStep == 0:
            run = np.int(t // par.SaveStep)
            s.SaveC[run+1] = s.c
            s.SaveW[run+1] = s.w
            s.SaveT[run+1] = T
            s.PlotStates(par, run)
            s.CheckC()
            print('{} Time: {:.2f}s, Position: {:.4f}'.format(run, time.clock()- par.start, s.w[par.N1]))
