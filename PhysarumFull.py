#!/usr/bin/python3
import numpy as np
from scipy import linalg
import argparse
import scipy.sparse.linalg as spla

# own files
from classes.state import State
from classes.param import Param
from classes.matrices import Matrices


def BuildMatrix(D1, D2, n):
    ATemp = np.zeros((n, n))
    mat.Factor(ATemp, 0, 0, -1.) #U->U
    mat.Factor(ATemp, 0, 1, 1./par.dt) #u->U
    mat.Dx(ATemp, 1, 0, D2, par.FracGel*par.VisGel) #U->u
    mat.Factor(ATemp, 1, 0, -par.FracGel*par.gammaG) #U->u
    mat.Dx(ATemp, 1, 1, D2, par.FracGel*par.mu) #u->u
    mat.Dx(ATemp, 1, 2, D2, par.FracSol*par.VisSol) #v->u
    mat.Dx(ATemp, 1, 3, D1, -1.) #P->u
    mat.Factor(ATemp, 2, 0, par.FracGel*par.beta) #U->v
    mat.Dx(ATemp, 2, 2, D2, par.VisSol) #v->v
    mat.Factor(ATemp, 2, 2, -par.FracGel*par.beta) #v->v
    mat.Dx(ATemp, 2, 3, D1, -1.) #P->v
    mat.Dx(ATemp, 3, 0, D1, par.FracGel) #U->P
    mat.Dx(ATemp, 3, 2, D1, par.FracSol) #v->P
    mat.Factor(ATemp, 3, 3, 0.0000000000001) #P->P5
    
    # Boundary conditions
    mat.ResetLine(ATemp, par.N1)
    mat.ResetLine(ATemp, 2*par.N1  - 1)
    mat.ResetLine(ATemp, 2*par.N1)
    mat.ResetLine(ATemp, 3*par.N1-1)
    if par.Bound == 0: #free
        ATemp[par.N1, :par.N1]     = par.FracGel * par.VisGel * D1[0] # dx U
        ATemp[par.N1, par.N1:2*par.N1] = par.FracGel * par.mu * D1[0] # dx u
        ATemp[par.N1, 2*par.N1:3*par.N1] = par.FracSol * par.VisSol * D1[0] # dx v
        ATemp[par.N1, 3*par.N1] = -1. #-p(0) or -p(L)

        ATemp[2*par.N1-1, :par.N1]    = par.FracGel * par.VisGel * D1[-1]
        ATemp[2*par.N1-1, par.N1:2*par.N1] = par.FracGel * par.mu * D1[-1]
        ATemp[2*par.N1-1, 2*par.N1:3*par.N1] = par.FracSol * par.VisSol * D1[-1]
        ATemp[2*par.N1-1, -1] = -1.
        
        ATemp[2*par.N1, 0] = 1. #U(0) == v(0)
        ATemp[2*par.N1, 2*par.N1] = -1.
        ATemp[3*par.N1-1, par.N1-1] = 1. #U(L) == v(L)
        ATemp[3*par.N1-1, 3*par.N1-1] = -1.
    elif par.Bound == 1: #dirichlet 0
        ATemp[par.N1    , par.N1] = 1. #u(0) = 0
        ATemp[2*par.N1-1, 2*par.N1-1] = 1. #u(L) = 0
        ATemp[2*par.N1, 2*par.N1] = 1. #v(0) = 0
        ATemp[3*par.N1-1, 3*par.N1-1] = 1.#v(L) = 0
    return ATemp


# commandline arguments
parser = argparse.ArgumentParser(description='Simulate Physarum microplasmodia movements!')
parser.add_argument("inputDir", help="The param.ini file should be located here")
args = parser.parse_args()
path = args.inputDir

par = Param(path)
par.VarNum = 4
np.random.seed(par.seed)
mat = Matrices(par)

Mech = BuildMatrix(mat.CD, mat.CD2, par.VarNum*par.N1)
MechInv = linalg.pinv2(Mech)

M2 = mat.Prec(Mech)

state = State(par)
state.SaveStep(par, 0, np.zeros(par.N1))

# Timestepping
for t in range(par.Steps):
    Adv_Diff = np.dot(mat.Ddt, np.diagflat(state.w[2*par.N1:3*par.N1] - state.w[:par.N1])) + mat.DC
    CNew, info = spla.gmres(Adv_Diff, state.c, M=mat.Prec(Adv_Diff), tol=par.tol)
    
    T = - par.xi * CNew/(1.+CNew)
    state.SetRHS(par, mat.CD, T)
    
    wNew, info = spla.gmres(Mech, state.rhs, x0=np.dot(MechInv, state.rhs.T), tol=par.tol, M=M2)
    
    CNew, state.c = state.c, CNew
    wNew, state.w = state.w, wNew
    
    state.SaveStep(par, t, np.dot(mat.CD, T))

state.SpaceTime(par)
state.DumpState(par)
state.PlotPos(par)
