import configparser
import numpy as np
import os
import sys
import inspect

def get_script_dir(follow_symlinks=True):
	if getattr(sys, 'frozen', False): # py2exe, PyInstaller, cx_Freeze
		path = os.path.abspath(sys.executable)
	else:
		path = inspect.getabsfile(get_script_dir)
	if follow_symlinks:
		path = os.path.realpath(path)
	return os.path.dirname(path)
	
def Sim():
	if condor:
		workdir = str(get_script_dir())
	else:
		workdir = get_script_dir()
	
	Pe = xi/(beta * Dcon)
	M = 2.* mu * FracGel/(beta * Dcon)
	#folder = os.path.join(str(round(Pe, 2)))
	folder = os.path.join(str(round(Pe, 2))+'_mu_'+str(mu))
	#~ folder = os.path.join('A_'+str(A)+'_B_'+str(B)+'_psi_'+str(psi)+'v1_'+str(v1)+'_al_'+str(al))
	print( folder)
	#create directories
	SimDir = os.path.join(workdir,path,folder)
	if not os.path.exists(SimDir):
		os.makedirs(SimDir)
	if not os.path.exists(os.path.join(SimDir, 'State')) and PlotStates:
		os.makedirs(os.path.join(SimDir, 'State'))
	if os.path.exists(os.path.join(SimDir, 'Crash.WTF')):
		os.remove(os.path.join(SimDir, 'Crash.WTF'))

	config = configparser.RawConfigParser()
	config.add_section('main')
	config.set('main', 'N', N)
	config.set('main', 'LengthX', LengthX)
	config.set('main', 'dt', dt)
	config.set('main', 'Dcon', Dcon)
	config.set('main', 'VisGel', VisGel)
	config.set('main', 'VisSol', VisSol)
	config.set('main', 'FracGel', FracGel)
	config.set('main', 'beta', beta)
	config.set('main', 'mu', mu)
	config.set('main', 'xi', xi)
	config.set('main', 'gammaG', gammaG)
	config.set('main', 'EndTime', EndTime)
	config.set('main', 'SaveTime', SaveTime)
	config.set('main', 'tol', tol)
	config.set('main', 'Pe', Pe)
	config.set('main', 'M', M)
	config.set('main', 'PlotStates', PlotStates)
	config.set('main', 'seed', run)
	config.set('main', 'Bound', Bound)
	
	with open(os.path.join(SimDir, 'param.ini'), 'w') as configfile:
		config.write(configfile)
	
	if condor:
		os.system('qsub -mem 2 -w %s -args %s -m e -env PYTHONPATH="$PYTHONPATH:." PhysarumFull.py' % (workdir, SimDir ) )
	else:
		os.system('python3 %s/PhysarumFull.py %s' % (get_script_dir(),os.path.join(path,folder) ) )

#PRL
N = 60
LengthX = 125.
dt = 0.01
Dcon = 200
VisGel = 1e-02
VisSol = 2.*1e-08
FracGel = 0.5
beta = 1e-04
mu = 0.01
xi = 0.12
path = 'Simulations/ILU'


PlotStates = False
tol = 1e-09

gammaG = 1e-05

run=0
EndTime = 750.
SaveTime = 2.
Prec = 1 #0 = no, 1 = ILU
Bound=0 #0 moving, 1 dirichlet 0
condor = False


#~ Sim()
# ~ for xi in np.arange(0.1,0.26,0.01): #start, stop, step
# ~ for xi in [0.115,0.12,0.125,0.13,0.135,0.14]: #start, stop, step
	# ~ for FracGel in [0.3, 0.4, 0.6, 0.7]:
	# ~ for al in [0.01,0.05,0.1,0.25,0.5]:
	# ~ for mu in [0.015,0.016,0.017,0.018,0.019,0.02,0.021,0.022,0.023,0.024,0.025,0.026,0.027,0.028,0.029,0.03,0.031,0.032]:
	# ~ for gammaG in [1e-09,5e-09]:
Sim()

